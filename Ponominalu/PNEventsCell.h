//
//  PNEventsCell.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 02.11.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kHeightCell  (isScreenS ? 44.0 : 52.0)

@interface PNEventsCell : UITableViewCell

@property (strong, nonatomic) PNEventsObject *eventsObj;

@end
