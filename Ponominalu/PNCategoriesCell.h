//
//  PNCategoriesCell.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 16.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kHeightCell  (isScreenS ? 44.0 : 52.0)

@protocol PNCategoriesCellProtocol

@end

@interface PNCategoriesCell : UITableViewCell

@property (weak, nonatomic) NSObject <PNCategoriesCellProtocol> *delegate;
@property (strong, nonatomic) PNCategoriesObject *categoriesObj;


@end
