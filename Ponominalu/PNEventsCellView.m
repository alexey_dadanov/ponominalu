//
//  PNEventsView.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 02.11.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNEventsCellView.h"

@implementation PNEventsCellView

- (instancetype)init {
    if (self = [super init]) {
        
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[PNImageFormatter deformationImageBackground:[UIImage imageNamed:@"bg_substrate"]]];
        [self addSubview:backgroundView];
        [backgroundView alignTop:@"-2" leading:@"2" bottom:@"2" trailing:@"-2" toView:self];
        
        _titleLabel = [[UILabel alloc]initWithFont:kFontHelveticaNeueMedium(IS_IPHONE_S ? 17.0 : 20.0)
                                      andTextColor:kColorTextWhite];
        [self addSubview:_titleLabel];
        [_titleLabel alignLeadingEdgeWithView:backgroundView predicate:@"12"];
        [_titleLabel alignTopEdgeWithView:backgroundView predicate:@"4"];
    }
    return self;
}

@end
