//
//  PNEventsCell.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 02.11.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNEventsCell.h"
#import "PNEventsCellView.h"

@implementation PNEventsCell {
    PNEventsCellView *cellView;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        
        cellView = [PNEventsCellView new];
        [self.contentView addSubview:cellView];
        [cellView alignTop:@"0" leading:@"0" bottom:@"0" trailing:@"0" toView:self.contentView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    cellView.alpha = (selected) ? 0.4 : 1.0;
}

- (void)setEventsObj:(PNEventsObject *)eventObj {
    _eventsObj = eventObj;
    
    cellView.titleLabel.attributedText = [PNHelper createAttStringWithString:eventObj.title
                                                                    withFont:cellView.titleLabel.font
                                                                   withColor:cellView.titleLabel.textColor
                                                                  withShadow:[PNConstants shadow]];
}

@end
