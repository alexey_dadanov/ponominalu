//
//  PNTextConstants.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#ifndef PNTextConstants_h
#define PNTextConstants_h

#define kFontHelveticaNeue(s)           [UIFont fontWithName:@"Helvetica Neue" size:s]
#define kFontHelveticaNeueLight(s)      [UIFont fontWithName:@"HelveticaNeue-Light" size:s]
#define kFontHelveticaNeueBold(s)       [UIFont fontWithName:@"HelveticaNeue-Bold" size:s]
#define kFontHelveticaNeueMedium(s)     [UIFont fontWithName:@"HelveticaNeue-Medium" size:s]

#endif /* PNTextConstants_h */
