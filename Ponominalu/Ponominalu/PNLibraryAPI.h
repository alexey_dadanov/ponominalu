//
//  PNLibraryAPI.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PNLibraryAPI : NSObject

+ (instancetype)sharedInstance;

@property (strong, nonatomic) PNHTTPClient *httpClient;

- (void)showApiAlertWithMessage:(NSString *)message;

@end
