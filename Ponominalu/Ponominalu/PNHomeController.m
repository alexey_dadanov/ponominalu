//
//  HomeViewController.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 13.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNHomeController.h"
#import "PNHomeView.h"
#import "PNCategoriesController.h"

@interface PNHomeController () <buttonProtocol> {
    PNHomeView  *cntrllrView;
}

@end

@implementation PNHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareController];
    cntrllrView.delegate = self;
    [self.navigationController.navigationBar setHidden:YES];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
}

- (void)prepareController {
    cntrllrView = [PNHomeView new];
    [self.view addSubview:cntrllrView];
    [cntrllrView alignTop:@"0" leading:@"0" bottom:@"0" trailing:@"0" toView:self.view];

}

- (void)buttonClicked {
    PNCategoriesController *categories = [PNCategoriesController new];
    [self.navigationController pushViewController:categories animated:YES];
    
}



@end
