//
//  PNLibraryAPI.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNLibraryAPI.h"

@implementation PNLibraryAPI

+ (instancetype)sharedInstance {
    static PNLibraryAPI *libraryApi = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        libraryApi = [[self alloc] init];
    });
    
    return libraryApi;
}

- (id)init {
    _httpClient = [[PNHTTPClient alloc]init];
    
    return self;
}

- (void)showApiAlertWithMessage:(NSString *)message {
    UIViewController* targetVC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    while (targetVC.presentedViewController) {
        targetVC = targetVC.presentedViewController;
    }
    
    if ([targetVC isKindOfClass:[UIAlertController class]]) {
        return;
    }
}

@end
