//
//  PNCategoriesViewController.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNCategoriesController.h"
#import "PNCategoriesView.h"
#import "PNCategoriesObject.h"
#import "PNCategoriesCell.h"
#import "PNDetailsController.h"
#import "PNGetEventsRequest.h"
#import "PNEventsController.h"

@interface PNCategoriesController () <UITableViewDelegate, UITableViewDataSource, PNCategoriesCellProtocol> {
    PNCategoriesView    *cntrllrView;
    NSArray             *getListArray;
}

@end

@implementation PNCategoriesController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareController];
    [self.navigationController.navigationBar setHidden:NO];
    [self getList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [cntrllrView.tableView reloadData];
}

- (void)prepareController {
    cntrllrView = [PNCategoriesView new];
    [self.view addSubview:cntrllrView];
    [cntrllrView alignTop:@"0" leading:@"0" bottom:@"0" trailing:@"0" toView:self.view];
    
    cntrllrView.tableView.delegate = self;
    cntrllrView.tableView.dataSource = self;
    [cntrllrView.tableView registerClass:[PNCategoriesCell class] forCellReuseIdentifier:@"categoriesCell"];
    cntrllrView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cntrllrView.tableView.rowHeight = kHeightCell;
    
    UIView *tableClearView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 2.0)];
    tableClearView.backgroundColor = [UIColor clearColor];
    cntrllrView.tableView.tableHeaderView = tableClearView;
    cntrllrView.tableView.tableFooterView = tableClearView;
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController.navigationBar setHidden:YES];
}

- (void) getList {
    [[PNLibraryAPI sharedInstance].httpClient getCategoriesWithCompletion:^(BOOL success, NSArray *responseArray, NSError *error) {
        if (success) {
            getListArray = responseArray;
            NSLog(@"%@", getListArray);
            [cntrllrView.tableView reloadData];
        } else if (error) {
            //алерт
        }
        
    }];
}



#pragma mark TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return getListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PNCategoriesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"categoriesCell" forIndexPath:indexPath];
    cell.categoriesObj = getListArray[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self getEventsForIndex:indexPath.row];
}

-(void)getEventsForIndex:(NSInteger)index {
    PNCategoriesObject *cObj = getListArray[index];
    
    [[PNLibraryAPI sharedInstance].httpClient getEventsOnRequest:[[PNGetEventsRequest alloc] initWithCategoryId:cObj.idCategory] withCompletion:^(BOOL success, PNEventsObject *responseObject, NSError *error) {
        if (success) {
            PNEventsController *eventsCntrllr = [PNEventsController new];
            eventsCntrllr.categories = cObj;
            eventsCntrllr.events = responseObject;
            [self.navigationController pushViewController:eventsCntrllr animated:YES];
        } else if (error) {
            //alert
        }
        [cntrllrView.tableView deselectRowAtIndexPath:[cntrllrView.tableView indexPathForSelectedRow] animated:YES];
        
    }];
}


@end
