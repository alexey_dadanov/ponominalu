//
//  PNHomeView.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNHomeView.h"

@implementation PNHomeView

- (instancetype)init {
    if (self = [super init]) {
        
        UIImageView *backgroundImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"menu_background"]];
        [self addSubview:backgroundImg];
        [backgroundImg alignTop:@"0" leading:@"0" bottom:@"0" trailing:@"0" toView:self];
        
        _enterButton = [UIButton buttonWithType:UIButtonTypeCustom];
        NSAttributedString *attString = [PNHelper createAttStringWithString:@"Войти"
                                                                    withFont:kFontHelveticaNeue(isScreenS ? 18.0 : 25.0)
                                                                   withColor:[UIColor blackColor]
                                                                  withShadow:[PNConstants shadow]];
        [_enterButton setAttributedTitle:attString forState:UIControlStateNormal];
        [_enterButton setBackgroundColor:KColorYellow];
        _enterButton.layer.cornerRadius = 15;
        [self addSubview:_enterButton];
        [_enterButton alignBottomEdgeWithView:backgroundImg predicate:@"-100"];
        [_enterButton alignCenterXWithView:backgroundImg predicate:@"0"];
        [_enterButton constrainWidth:isScreenS ? @"290":@"330" height:isScreenS ? @"50":@"60"];
        
        [_enterButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];

    }
    return self;
}

- (void)buttonTapped:(id)sender {
    [self.delegate buttonClicked];
}

@end
