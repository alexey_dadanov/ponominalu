//
//  PNHTTPClient.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PNGetEventsRequest.h"

@interface PNHTTPClient : NSObject <NSURLConnectionDelegate>

+ (AFHTTPSessionManager *)sharedManager;

+ (void)cancelAllRequests;
+ (void)cancelLastRequest;


- (void)getCategoriesWithCompletion:(void(^)(BOOL success, NSArray *responseArray, NSError *error))completion;
- (void)getEventsOnRequest:(PNGetEventsRequest *)request withCompletion:(void(^)(BOOL success, PNEventsObject *responseObject, NSError *error))completion;

@end
