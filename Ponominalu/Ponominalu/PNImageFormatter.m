//
//  PNImageFormatter.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 16.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNImageFormatter.h"

@implementation PNImageFormatter

+ (UIImage *)deformationImageBackground:(UIImage *)image forInset:(CGFloat)inset {
    UIEdgeInsets insets = UIEdgeInsetsMake(inset, inset, inset, inset);
    UIImage *stretchableImage = [image resizableImageWithCapInsets:insets];
    
    return stretchableImage;
}

+ (UIImage *)deformationImageBackground:(UIImage *)image {
    return [self.class deformationImageBackground:image forInset:32.0];
}

@end
