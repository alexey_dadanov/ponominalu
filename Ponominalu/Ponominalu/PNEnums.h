//
//  PNEnums.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#ifndef PNEnums_h
#define PNEnums_h

typedef NS_ENUM(NSInteger, ScreenIPhone) {
    ScreenIPhone4,
    ScreenIPhone5,
    ScreenIPhone6,
    ScreenIPhone6p
};

#endif /* PNEnums_h */
