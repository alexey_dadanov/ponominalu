//
//  AppDelegate.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 13.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

