//
//  PNDetailsObject.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 16.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNEventsObject.h"

@implementation PNEventsObject

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
        if ([dictionary isKindOfClass:[NSDictionary class]]) {
            _eventId = [PNHelper readNumber:[dictionary valueForKey:@"id"]];
            _title =   [PNHelper readString:[dictionary valueForKey:@"title"]];
            
            id array = [dictionary objectForKey:@""];
            if (![array isKindOfClass:[NSNull class]] && [array isKindOfClass:[NSArray class]]) {
                NSMutableArray *mArray = [[NSMutableArray alloc]init];
                
                for (int i=0; i < ((NSArray *)array).count; i++) {
                    [mArray addObject: [[PNEventsObject alloc]initWithDictionary:array[i]]];
                }
                _eventsArray = mArray;
            }
        }
    return self;
}

@end
