//
//  PNEventsViewController.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNEventsController.h"
#import "PNEventsView.h"
#import "PNEventsObject.h"
#import "PNEventsCell.h"

@interface PNEventsController () <UITableViewDelegate, UITableViewDataSource> {
    PNEventsView *cntrllrView;
    NSArray      *getArray;
}


@end

@implementation PNEventsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareController];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:NO];
    [cntrllrView.tableView reloadData];
}

- (void)prepareController {
    cntrllrView = [PNEventsView new];
    [self.view addSubview:cntrllrView];
    [cntrllrView alignTop:@"0" leading:@"0" bottom:@"0" trailing:@"0" toView:self.view];
    
    cntrllrView.tableView.delegate = self;
    cntrllrView.tableView.dataSource = self;
    [cntrllrView.tableView registerClass:[PNEventsCell class] forCellReuseIdentifier:@"eventsCell"];
    
}

- (void)viewWillDisappear:(BOOL)animated {
        [self.navigationController.navigationBar setHidden:NO];
}

#pragma mark TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 20;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PNEventsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"eventsCell" forIndexPath:indexPath];
    cell.eventsObj = getArray[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

@end
