//
//  PNHTTPClient.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNHTTPClient.h"
#import "PNCategoriesObject.h"
#import "PNGetEventsRequest.h"
#import "PNEventsObject.h"

@implementation PNHTTPClient

// shared operation manager
+ (AFHTTPSessionManager*)sharedManager {
    static AFHTTPSessionManager *sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [AFHTTPSessionManager manager];
        sharedManager.responseSerializer = [AFJSONResponseSerializer serializer];
        sharedManager.securityPolicy.allowInvalidCertificates = NO;
        
    });
    
    return sharedManager;
}

+ (void)cancelAllRequests {
    for (NSURLSessionDataTask *task in [[self sharedManager] dataTasks]) {
        [task cancel];
    }
}

+ (void)cancelLastRequest {
    NSURLSessionDataTask *task = [[self sharedManager] dataTasks].lastObject;
    [task cancel];
}

- (BOOL)checkGlobalErrorCodesForError:(NSError *)error {
    NSLog(@"Error: %@", error);
    switch (error.code) {
        case NSURLErrorBadServerResponse:
        case NSURLErrorNotConnectedToInternet:
        case kCFURLErrorTimedOut:
        case kCFURLErrorCannotConnectToHost:
            [[PNLibraryAPI sharedInstance] showApiAlertWithMessage:error.userInfo[@"NSLocalizedDescription"]];
            return YES;
        case NSURLErrorCancelled:
            return YES;
            break;
        default:
            break;
    }
    return NO;
}

- (void)getCategoriesWithCompletion:(void(^)(BOOL success, NSArray *responseArray, NSError *error))completion {
    
    //получаем json категорий
    NSString *urlString = [NSString stringWithFormat:@"%@/v4/categories/list", BASE_SERVER_URL];
    NSDictionary *parameters = @{ @"session" : @"sesson_iphone_2015_ponominalu_msk" };
    [[self.class sharedManager] GET:urlString parameters:parameters
                           progress:^(NSProgress * _Nonnull downloadProgress) {}
                            success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                DLog(@"JSON: %@", responseObject);
                                NSMutableArray *finishArray;
                                NSArray *result = [responseObject objectForKey:@"message"];
                                
                                if ([result isKindOfClass:[NSArray class]]) {
                                    finishArray = [NSMutableArray new];
                                    for (NSDictionary *dict in result) {
                                        [finishArray addObject:[[PNCategoriesObject alloc]initWithDictionary:dict]];
                                    }
                                }
                                
                                completion(YES, finishArray, nil);
//                                NSLog(@"%@", finishArray);
                            }
                            failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                if ([self checkGlobalErrorCodesForError:error]) {
                                    completion(NO, nil, nil);
                                } else {
                                    completion(NO, nil, error);
                                }
                            }];
}

- (void)getEventsOnRequest:(PNGetEventsRequest *)request withCompletion:(void(^)(BOOL success, PNEventsObject *responseObject, NSError *error))completion {
    NSString *urlString = [NSString stringWithFormat:@"%@/v4/events/list", BASE_SERVER_URL];
    
    [[self.class sharedManager] GET:urlString parameters:request.parameters
                           progress:^(NSProgress * _Nonnull downloadProgress) {}
                            success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                DLog(@"JSON: %@", responseObject);
                                NSLog(@"%@", responseObject);
                                PNEventsObject *responseObj;
                                id dict = [responseObject objectForKey:@"message"];
                                if ([dict isKindOfClass:[NSDictionary class]]) {
                                    responseObj = [[PNEventsObject alloc]initWithDictionary:dict];
                                }
                                
                                completion(YES, responseObj, nil);
                            }
                            failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                if ([self checkGlobalErrorCodesForError:error]) {
                                    completion(NO, nil, nil);
                                } else {
                                    completion(NO, nil, error);
                                }
                            }];
}


@end
