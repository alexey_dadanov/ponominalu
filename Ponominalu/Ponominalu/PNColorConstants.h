//
//  PNColorConstants.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#ifndef PNColorConstants_h
#define PNColorConstants_h

#define Rgb2UIColor(r, g, b)                                [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define Rgba2UIColor(r, g, b, a)                            [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:a]

#define kColorBackgroundRedWine                               Rgb2UIColor(193, 10, 21)
#define kColorBackgroundBlack                                 Rgb2UIColor(24, 24, 24)

#define kColorTextDark                                        Rgb2UIColor(24, 24, 24)

#define kColorShadowBlack                                     Rgba2UIColor(0, 0, 0, 0.3)

#define kColorTextDarkGray                                  Rgb2UIColor(74, 74, 74)
#define kColorTextWhite                                     Rgb2UIColor(255, 255, 255)
#define kColorTextGreen                                     Rgb2UIColor(113, 202, 82)
#define kColorTextDarkGreen                                 Rgb2UIColor(78, 172, 46)
#define kColorTextBlue                                      Rgb2UIColor(27, 149, 199)
#define kColorTextYellow                                    Rgb2UIColor(248, 231, 28)
#define kColorTextRed                                       Rgb2UIColor(232, 74, 48)

#define kColorViolet                                        Rgb2UIColor(184,166,228)
#define KColorYellow                                        Rgb2UIColor(255,236,87)
#define kColorBackgroundWhite                               Rgb2UIColor(255, 255, 255)
#define kColorBackgroundGreen                               Rgb2UIColor(113, 202, 82)
#define kColorBackgroundBlue                                Rgb2UIColor(27, 149, 199)
#define kColorBackgroundLoader                              Rgba2UIColor(0, 0, 0, 0.5)

#endif /* PNColorConstants_h */
