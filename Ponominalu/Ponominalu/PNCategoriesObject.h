//
//  PNCategoriesObject.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PNCategoriesObject : NSObject

@property (strong, nonatomic) NSNumber *idCategory;     //id
@property (strong, nonatomic) NSString *title;          //Название категории рус
@property (strong, nonatomic) NSString *alias;          //Название категории англ
@property (strong, nonatomic) NSNumber *events_count;   //Колличество событий в категории

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

- (void)encodeWithCoder:(NSCoder *)encoder;
- (instancetype)initWithCoder:(NSCoder *)decoder;

@end
