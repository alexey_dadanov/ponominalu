//
//  PNGetDetailRequest.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 16.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PNGetEventsRequest : NSObject

- (instancetype)initWithCategoryId:(NSNumber *)categId;

@property (nonatomic, strong) NSDictionary *parameters;

@end
