//
//  PNConstants.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNConstants.h"

@interface PNConstants ()  {
    BOOL isIphone4;
    BOOL isIphone5;
    BOOL isIphone6;
    BOOL isIphone6p;
}
@end

@implementation PNConstants

+ (instancetype)shared {
    static PNConstants *constants = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        constants = [[self alloc] init];
    });
    
    return constants;
}

- (id)init {
    [self prepareConstantsObjects];
    return self;
}

- (void)prepareConstantsObjects {
    [self prepareShadow];
    [self prepareSizes];
}

// Sizes
- (void)prepareSizes {
    isIphone4 = (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)480) < DBL_EPSILON);
    isIphone5 = (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)568) < DBL_EPSILON);
    isIphone6 = (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON);
    isIphone6p = (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON);
    
    _mainScreenBounds = [[UIScreen mainScreen]bounds];
}

- (BOOL)isScreenIPhone:(NSInteger)iphone {
    switch (iphone) {
        case ScreenIPhone4:
            return isIphone4;
        case ScreenIPhone5:
            return isIphone5;
        case ScreenIPhone6:
            return isIphone6;
        case ScreenIPhone6p:
            return isIphone6p;
    }
    return NO;
}

// Shadow
- (void)prepareShadow {
    _shadow = [NSShadow new];
    _shadow.shadowColor = kColorShadowBlack;
    _shadow.shadowOffset = CGSizeMake(0, 2.0);
    _shadow.shadowBlurRadius = 4.0;
}

+ (NSShadow *)shadow {
    return [PNConstants shared].shadow;
}

@end
