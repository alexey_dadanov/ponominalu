//
//  PNCategoriesView.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PNCategoriesView : UIView

@property (nonatomic, strong) UITableView *tableView;

@end
