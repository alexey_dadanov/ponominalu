//
//  PNDetailsObject.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 16.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PNCategoriesObject;

@interface PNEventsObject : NSObject

@property (strong, nonatomic) NSNumber *eventId;        //id
@property (strong, nonatomic) NSString *title;          //Название
@property (strong, nonatomic) NSArray  *eventsArray;    //Массив

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
