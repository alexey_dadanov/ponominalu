//
//  PNHelper.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PNHelper : NSObject

+ (NSNumber *)readNumber:(id)number;
+ (NSDecimalNumber *)readDecimalNumber:(id)number;
+ (NSString *)readString:(id)string;
+ (BOOL)readBoolean:(id)boolean;

+ (NSString *)encodeUrlString:(NSString *)string;
+ (NSString *)decodeUrlString:(NSString *)string;

+ (NSInteger)checkIndexVisibleTextForString:(NSString *)string withFont:(UIFont *)font withHeight:(CGFloat)height withWidth:(CGFloat)width;

+ (NSString *)wrapString:(NSString *)string;

+ (NSString *)getApplicationVersion;
+ (NSString *)getApplicationBuild;

+ (NSAttributedString *)createAttStringWithString:(NSString *)string withFont:(UIFont *)font withColor:(UIColor *)color withShadow:(NSShadow *)shadow;
+ (NSAttributedString *)attStringAddLinkText:(NSString *)text withUrlString:(NSString *)URL toAttributedString:(NSAttributedString *)attString;

+ (CGFloat)getImageViewBorderWidthForImage:(UIImage *)image;

@end
