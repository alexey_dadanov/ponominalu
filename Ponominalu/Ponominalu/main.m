//
//  main.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 13.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PNAppDelegate class]));
    }
}
