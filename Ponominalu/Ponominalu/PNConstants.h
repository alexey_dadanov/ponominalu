//
//  PNConstants.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef DEBUG
#   define DLog(...) NSLog(__VA_ARGS__)
#else
#   define DLog(...) /* */
#endif

#define kMainScreenBounds                                   [PNConstants shared].mainScreenBounds

//S, M, L, XL: Small, Medium, Large, Extra Large
#define isScreenS                                           (IS_IPHONE_4 || IS_IPHONE_5)
#define isScreenM                                           (IS_IPHONE_6)
#define isScreenL                                           (IS_IPHONE_6P)
#define IS_IPHONE_S                                         (IS_IPHONE_4 || IS_IPHONE_5)
#define IS_IPHONE_4                                         [[PNConstants shared] isScreenIPhone:ScreenIPhone4]
#define IS_IPHONE_5                                         [[PNConstants shared] isScreenIPhone:ScreenIPhone5]
#define IS_IPHONE_6                                         [[PNConstants shared] isScreenIPhone:ScreenIPhone6]
#define IS_IPHONE_6P                                        [[PNConstants shared] isScreenIPhone:ScreenIPhone6p]

#define BASE_SERVER_URL                                      @"https://api.cultserv.ru"

@interface PNConstants : NSObject

+ (instancetype)shared;

- (BOOL)isScreenIPhone:(NSInteger)iphone;
@property (nonatomic, assign) CGRect mainScreenBounds;

@property (nonatomic, strong) NSShadow *shadow;
+ (NSShadow *)shadow;

@end
