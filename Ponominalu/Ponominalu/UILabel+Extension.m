//
//  UILabel+Extension.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 16.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "UILabel+Extension.h"

@implementation UILabel (Extension)

- (instancetype)initWithFont:(UIFont *)font andTextColor:(UIColor *)color {
    if (self = [super init]) {
        
        self.font = font;
        self.textColor = color;
        
    }
    return self;
}

@end
