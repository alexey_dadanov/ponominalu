//
//  PNHomeView.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol buttonProtocol

- (void)buttonClicked;

@end

@interface PNHomeView : UIView

@property (nonatomic, strong) UIButton *enterButton;
@property (nonatomic, assign) id <buttonProtocol> delegate;

@end
