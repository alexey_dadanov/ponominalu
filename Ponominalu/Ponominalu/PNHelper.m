//
//  PNHelper.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNHelper.h"

@implementation PNHelper

+ (NSNumber *)readNumber:(id)number {
    if ([number isKindOfClass:[NSNumber class]]) {
        return number;
    } else if ([number isKindOfClass:[NSString class]]) {
        return [NSNumber numberWithLongLong:((NSString *)number).longLongValue];
    } else {
        return nil;
    }
}

+ (NSDecimalNumber *)readDecimalNumber:(id)number {
    if ([number isKindOfClass:[NSDecimalNumber class]]) {
        return number;
    } else if ([number isKindOfClass:[NSNumber class]]) {
        return [NSDecimalNumber decimalNumberWithDecimal:[number decimalValue]];
    } else {
        return nil;
    }
}

+ (NSString *)readString:(id)string {
    if ([string isKindOfClass:[NSString class]]) {
        return string;
    } else if ([string isKindOfClass:[NSDecimalNumber class]]) {
        return [(NSDecimalNumber*)string stringValue];
    } else if ([string isKindOfClass:[NSNumber class]]) {
        return [(NSNumber*)string stringValue];
    } else {
        return nil;
    }
}

+ (BOOL)readBoolean:(id)boolean {
    if ([boolean isKindOfClass:[NSNumber class]]) {
        return [(NSNumber*)boolean boolValue];
    } else {
        return NO;
    }
}

+ (NSString *)encodeUrlString:(NSString *)string {
    return CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                     kCFAllocatorDefault,
                                                                     (__bridge CFStringRef)string,
                                                                     NULL,
                                                                     CFSTR(":/?#[]@!$&'()*+,;="),
                                                                     kCFStringEncodingUTF8));
}

+ (NSString*)decodeUrlString:(NSString *)string {
    return CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(
                                                                                     kCFAllocatorDefault,
                                                                                     (__bridge CFStringRef)string,
                                                                                     CFSTR(""),
                                                                                     kCFStringEncodingUTF8));
}

+ (NSInteger)checkIndexVisibleTextForString:(NSString *)string withFont:(UIFont *)font withHeight:(CGFloat)height withWidth:(CGFloat)width {
    NSUInteger numberOfCharsInLabel = NSNotFound;
    
    for (int i = (int)string.length; i >= 0; i--) {
        NSString *substring = [string substringToIndex:i];
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:substring
                                                                             attributes:@{ NSFontAttributeName :font}];
        CGSize size = CGSizeMake(width, CGFLOAT_MAX);
        CGRect textFrame = [attributedText boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        
        if (CGRectGetHeight(textFrame) <= height) {
            numberOfCharsInLabel = i;
            break;
        }
    }
    
    return numberOfCharsInLabel;
}

+ (NSString *)getApplicationVersion {
    NSString *versionString = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    return versionString;
}
+ (NSString *)getApplicationBuild {
    NSString *buildString = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    
    return buildString;
}
+ (NSString *)wrapString:(NSString *)string{
    if ([string isEqualToString:@"Владимирводоканал"]){
        string = @"Владимирводо-\nканал";
    }
    if ([string isEqualToString:@"СК \"Луч\" (Электроприбор)"]) {
        string = @"СК «Луч»\n(Электроприбор)";
    }
    if ([string isEqualToString:@"ЭлектроМашКомплект"]){
        string = @"ЭлектроМаш-\nКомплект";
    }
    if ([string isEqualToString:@"Лицей-интернат № 1"]){
        string = @"Лицей-интернат\n№ 1";
    }
    
    return string;
}


+ (NSAttributedString *)createAttStringWithString:(NSString *)string withFont:(UIFont *)font withColor:(UIColor *)color withShadow:(NSShadow *)shadow {
    NSMutableAttributedString* attString = [[NSMutableAttributedString alloc] initWithString:(string) ?: @""];
    NSRange range = NSMakeRange(0, [attString length]);
    [attString addAttribute:NSFontAttributeName value:font range:range];
    [attString addAttribute:NSForegroundColorAttributeName value:color range:range];
    [attString addAttribute:NSShadowAttributeName value:shadow range:range];
    
    return attString;
}

+ (NSAttributedString *)attStringAddLinkText:(NSString *)text withUrlString:(NSString *)URL toAttributedString:(NSAttributedString *)attString {
    NSMutableAttributedString *mAttString = [[NSMutableAttributedString alloc] initWithAttributedString:attString];
    if (attString) {
        NSRange urlRange = [attString.string rangeOfString:text options:NSCaseInsensitiveSearch];
        if (urlRange.location != NSNotFound) {
            [mAttString addAttribute:NSLinkAttributeName value:URL range:urlRange];
            
            [mAttString enumerateAttributesInRange:NSMakeRange(0, mAttString.string.length)
                                           options:NSAttributedStringEnumerationReverse usingBlock:^(NSDictionary *attributes, NSRange range, BOOL *stop) {
                                               
                                               NSMutableDictionary *mutableAttributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
                                               UIFont *font = [mutableAttributes objectForKey:NSFontAttributeName];
                                               [mAttString addAttribute:NSFontAttributeName value:kFontHelveticaNeueBold(font.pointSize+2.0)  range:urlRange];
                                           }];
            
        }
    }
    return mAttString;
}
+ (CGFloat)getImageViewBorderWidthForImage:(UIImage *)image{
    if(image)
        return 1.0;
    else
        return 0.0;
}

@end
