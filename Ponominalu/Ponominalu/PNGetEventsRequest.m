//
//  PNGetDetailRequest.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 16.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNGetEventsRequest.h"

@implementation PNGetEventsRequest

- (instancetype)initWithCategoryId:(NSNumber *)categoryId {
    if (self = [super init]) {
        _parameters = @{@"session" : @"sesson_iphone_2015_ponominalu_msk",
                        @"category_id"  : categoryId.stringValue};
    }
    return self;
}

@end
