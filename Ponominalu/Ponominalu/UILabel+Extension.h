//
//  UILabel+Extension.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 16.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extension)

- (instancetype)initWithFont:(UIFont *)font andTextColor:(UIColor *)color;

@end
