//
//  PNCategoriesObject.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNCategoriesObject.h"

@implementation PNCategoriesObject

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        if ([dictionary isKindOfClass:[NSDictionary class]]) {
            _idCategory = [PNHelper readNumber:[dictionary valueForKey:@"id"]];
            _title = [PNHelper readString:[dictionary valueForKey:@"title"]];
            _alias = [PNHelper readString:[dictionary valueForKey:@"alias"]];
            _events_count = [PNHelper readNumber:[dictionary valueForKey:@"events_count"]];
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_title forKey:@"title"];
    [encoder encodeObject:_idCategory forKey:@"id"];
    [encoder encodeObject:_alias forKey:@"alias"];
    [encoder encodeObject:_events_count forKey:@"events_count"];
}

- (instancetype)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]){
        _idCategory = [decoder decodeObjectForKey:@"id"];
        _title = [decoder decodeObjectForKey:@"title"];
        _alias = [decoder decodeObjectForKey:@"alias"];
        _events_count = [decoder decodeObjectForKey:@"events_count"];
    }
    return self;
}

@end
