//
//  PNImageFormatter.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 16.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PNImageFormatter : NSObject

+ (UIImage *)deformationImageBackground:(UIImage *)image forInset:(CGFloat)inset;
+ (UIImage *)deformationImageBackground:(UIImage *)image;

@end
