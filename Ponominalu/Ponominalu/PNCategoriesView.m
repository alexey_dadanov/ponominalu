//
//  PNCategoriesView.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 15.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNCategoriesView.h"

@implementation PNCategoriesView

- (instancetype)init {
    if (self = [super init]) {
        
        UIImageView *backgroundImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"menu_background"]];
        [self addSubview:backgroundImg];
        [backgroundImg alignTop:@"0" leading:@"0" bottom:@"0" trailing:@"0" toView:self];
    
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:_tableView];
        [_tableView alignTop:@"0" leading:@"0" bottom:@"0" trailing:@"0" toView:self];
        
    }
    return self;

}

@end
