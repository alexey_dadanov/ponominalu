//
//  PNCategoriesCell.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 16.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNCategoriesCell.h"
#import "PNCategoriesCellView.h"

@implementation PNCategoriesCell {
    PNCategoriesCellView *cellView;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        
        cellView = [PNCategoriesCellView new];
        [self.contentView addSubview:cellView];
        [cellView alignTop:@"0" leading:@"0" bottom:@"0" trailing:@"0" toView:self.contentView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    cellView.alpha = (selected) ? 0.4 : 1.0;
}

- (void)setCategoriesObj:(PNCategoriesObject *)categObj {
    _categoriesObj = categObj;
    
    cellView.titleLabel.attributedText = [PNHelper createAttStringWithString:_categoriesObj.title
                                                                     withFont:cellView.titleLabel.font
                                                                    withColor:cellView.titleLabel.textColor
                                                                   withShadow:[PNConstants shadow]];
    if (_categoriesObj.events_count) {
        cellView.subTitleLabel.attributedText = [PNHelper createAttStringWithString:[NSString stringWithFormat:NSLocalizedString(@"Событий %@",), _categoriesObj.events_count.stringValue]
                                                                            withFont:cellView.subTitleLabel.font
                                                                           withColor:cellView.subTitleLabel.textColor
                                                                          withShadow:[PNConstants shadow]];
    } else {
        cellView.subTitleLabel.attributedText = nil;
    }
}


@end
