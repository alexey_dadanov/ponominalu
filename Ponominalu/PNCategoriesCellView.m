//
//  PNCategoriesCellView.m
//  Ponominalu
//
//  Created by Dadanov Alexey on 16.10.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import "PNCategoriesCellView.h"

@implementation PNCategoriesCellView

- (instancetype)init {
    if (self = [super init]) {
        
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[PNImageFormatter deformationImageBackground:[UIImage imageNamed:@"bg_substrate"]]];
        [self addSubview:backgroundView];
        [backgroundView alignTop:@"-2" leading:@"2" bottom:@"2" trailing:@"-2" toView:self];
        
        _titleLabel = [[UILabel alloc]initWithFont:kFontHelveticaNeueMedium(IS_IPHONE_S ? 17.0 : 20.0)
                                      andTextColor:kColorTextWhite];
        [self addSubview:_titleLabel];
        [_titleLabel alignLeadingEdgeWithView:backgroundView predicate:@"12"];
        [_titleLabel alignTopEdgeWithView:backgroundView predicate:@"4"];
        
        _subTitleLabel = [[UILabel alloc]initWithFont:kFontHelveticaNeue(IS_IPHONE_S ? 10.0 : 12.0)
                                         andTextColor:kColorTextWhite];
        [self addSubview:_subTitleLabel];
        [_subTitleLabel constrainTopSpaceToView:_titleLabel predicate:@"0"];
        [_subTitleLabel alignLeadingEdgeWithView:backgroundView predicate:@"12"];

    }
    return self;
}


@end
