//
//  PNEventsView.h
//  Ponominalu
//
//  Created by Dadanov Alexey on 02.11.16.
//  Copyright © 2016 Dadanov Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PNEventsCellView : UIView

@property (strong, nonatomic) UILabel *titleLabel;

@end
